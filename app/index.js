/* eslint-disable import/first */
import httpServer from './api/http/server';

/**
 * logger instance
 * @type {Object}
 */

process.on('unhandledRejection', (error) => {
  /* istanbul ignore next */
  console.error(`errors here: ${error}`);
});

let server;

(async () => {
  try {
    server = await httpServer.init();
    console.info('application initialized');
  } catch (error) {
    console.error({ message: 'Error while initializing application', error });
  }
})();

process.on('SIGTERM', () => {
  console.info('GOT SIGTERM, start shutdown.');
});
