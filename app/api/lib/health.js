import { WebError } from '@nsh/pmd-http-tools';
import rabbitmq from '@nsh/pmd-rabbitmq';
import esClient from '../elasticsearch/es-client';

/**
 * process state
 * @type {Object}
 */
const state = { isShutdown: false };

/**
 * Change state when SIGTERM is catched
 */
process.on('SIGTERM', () => {
  state.isShutdown = true;
});

/**
 * Check third dependencies
 * @throws {WebError} - error
 */
export default async function health() {
  if (state.isShutdown) {
    throw new WebError('Health not OK => process is shuting down.', 500);
  }
  try {
    await esClient.ping({ requestTimeout: 1000 });
    if (!await rabbitmq.ping()) {
      throw new WebError('Health not OK => lost amqp connection.', 500);
    }
  } catch (error) {
    throw new WebError(error.message, 500);
  }
}
