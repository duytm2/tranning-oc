import { loggerFactory } from '@nsh/logger';
import rabbitmq from '@nsh/pmd-rabbitmq';
import esClient from '../elasticsearch/es-client';

/**
 * logger instance
 * @type {Object}
 */
const logger = loggerFactory.getLogger(__filename);
/**
 * @constant
 * @type {number}
 * @default
 */
const READINESS_PROBE_DELAY = 5;

/**
 * Closing connections & process
 * @param {net.Server} server - express server instance
 */
function shutdown(server) {
  try {
    server.close(() => {
      esClient.close();
      rabbitmq.close();
    });
    logger.info('Shutdown succeeded.');
    process.exit(0);
  } catch (error) {
    logger.error({
      message: 'Shutdown failed.',
      error,
    });
    process.exit(1);
  }
}

export { READINESS_PROBE_DELAY, shutdown };
