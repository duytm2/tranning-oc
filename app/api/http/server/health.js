import express from 'express';
import asyncHandler from 'express-async-handler';

/**
 * express router instance for health entity
 */
const router = express.Router();

router.post('/', asyncHandler(async (req, res) => {
  const end = histogram.startTimer();
  const name = request.query.name ? request.query.name : 'World';
  response.send({content: `Hello, ${name}!`});
  // stop the timer
  end({ method: request.method, 'status_code': 200 });
}));

export default router;
