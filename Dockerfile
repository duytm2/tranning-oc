FROM nexus.neopost-id.com:4432/nsh-pmdpso-node:10.15.1
LABEL maintainer="PSO Team <DTL.SHIPPING.PSO@fsoft.com.vn>"
COPY --chown=nsh:nshgroup ./build /app/
COPY --chown=nsh:nshgroup ./node_modules /app/node_modules
EXPOSE 3000
CMD [ "npm", "start" ]
