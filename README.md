pmdpso-node-starter
====

This repository should be the seed project for all nodejs microservices of PMDPSO platform.

Feature list
----

- Latest ECMA standards (stage-0) support with Babel.
- ESLint for coding style validation with [PMDPSO standard](https://gitlab.neopost-id.com/packcity/pmd-pso/nexus-module/nsh-pmdpso-eslint).
- Duplicated code detection with [**jscpd**](https://npm.im/jscpd) utility.
- Document generation powered by **ESDoc**. Make sure your comments are conforming ESDoc standard.
- Testing utilities: chaijs, mocha for unit testing; cucumber (gherkin) for contract testing.
- Gitlab CI script is ready to be used.
- Dockerfile based on latest **nsh-pmdpso-node:10.15.1** docker image.
- [...]

Application health reporting and life cycles
----

- Make sure your microservice provides K8S a health (and maybe readiness) reporting mechanism (TCP/HTTP).
- Make sure your microservice responds to UNIX process signals like SIGTERM, SIGINT, etc. and provide a graceful shutdown to prevent data loss.
- Make sure your microservice provides useful metric data via Prometheus.

Directory structure
----

Here is a brief information about how our code should be organized.

- **app**: Actual source code of the microservice. You should keep in mind that only things inside this directory are included in built docker image.
  - **api**: Modules to interact with outside world (http, rabbitmq, redis).
  - **schemas**: Provide an AJV instance and related JSON schemas.
  - **services**: Business logic code goes here.
  - **index.js**: Application entrypoint.
- **test**: Directory of test and test resources.
  - **test/units**: Unit test should go here.
  - **test/contracts**: And contract test go here.
  - **test/data**: Payloads that might be needed in our tests.
- **resources**: bash scripts, Openshift definitions, etc.

How to start
----

First you have to define your new app name.

```bash
APP_NAME=my-new-app
```

Then in the same terminal window run the following script

```bash
git clone --depth 1 git@gitlab.neopost-id.com:packcity/pmd-pso/node-starter.git $APP_NAME && cd $APP_NAME \
  && find . -type f -exec sed -i -e "s/pmdpso-node-starter/${APP_NAME}/" "{}" ";" \
  && git add . && git commit -nm "Initialize ${APP_NAME}"
  && echo "Project ${APP_NAME} initialized"
```

Now you have to add a remote for new project. Remember to change origin remote URL here to your one.

```bash
git remote rm origin && git remote add origin "git@gitlab.neopost-id.com:packcity/pmd-pso/example.git"
```

Conventions
----

Make sure you have [**git-flow**](https://danielkummer.github.io/git-flow-cheatsheet/) installed and understand how it work.

Git commit messages should follow [**this format**](https://seesparkbox.com/foundry/semantic_commit_messages).

### Git branches and tags

- **develop**: development branch. Collaborator should send MR from features/hotfixes to this branch. Reviewed and maintained by Hanoi.
- **feature/some-nice-feature**: Your will work on each feature at a time and it should be done here. Later it will be merged into **develop** branch.
- **tags** (x.x.x): when the micro-service is stable and is behaving properly on Openshift dev environment, it can be released. Also Pipelines on tags will roll out new version of deploy books with this new tag.

Tests
---

### Unit Tests

```bash
npm run test
```

### Contract tests

Tests a deployed service through its interfaces. The tests should cover all technical specification.

```bash
npm run test:contract
```

_To run both unit tests and contract tests, then collect the coverage result, you can use following command:_

```bash
./resources/tests-and-coverage.sh
```

Current coverage threshold is 90% of lines should be covered.
