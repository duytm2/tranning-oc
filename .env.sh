# This file is used to inject some environment variables into current shell session. Do this every time you open a new shell.
# To use this file make sure you are using a sh-compliant shell
# Usage: `source .env.sh`

export COUNTRY=australia
export CARRIER=apo
export REDIS_HOST="pso.io"
export RABBIT_URL="amqp://guest:guest@pso.io/"

if [ -f ./.env.local.sh ]; then
  # This file hold private environment variables that should not be committed to VCS
  source .env.local.sh
fi

echo "Variables exported"
